
/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

extern crate rand;
extern crate nanoid;
extern crate md5;
extern crate strum;
#[macro_use]
extern crate strum_macros;

use rand::{thread_rng, Rng, ThreadRng};
use std::fmt;
use std::collections::HashMap;
use std::string::ToString;

fn throw_die(rng: &mut ThreadRng) -> u32 {
    let n = rng.gen_range(1, 7);
    return n;
}

struct UPP {
    strength: i32,
    dexterity: i32,
    endurance: i32,
    intelligence: i32,
    education: i32,
    social: i32
}

impl fmt::Display for UPP {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:X}{:X}{:X}{:X}{:X}{:X}",
            self.strength, self.dexterity, self.endurance,
            self.intelligence, self.education, self.social)
    }
}

trait Hits {
    fn calc_hits(&self) -> i32;
}

impl Hits for UPP {
    fn calc_hits(&self) -> i32 {
        self.strength + self.dexterity + self.endurance
    }
}

trait Career {
    fn rank_navy(&self) -> i32;
    fn rank_marines(&self) -> i32;
    fn rank_army(&self) -> i32;
    fn rank_scouts(&self) -> i32;
    fn rank_merchant(&self) -> i32;
    fn rank_other(&self) -> i32;
    fn enlist(&self, career: CareerType, rng: &mut ThreadRng) -> CareerType;
    fn draft(rng: &mut ThreadRng) -> CareerType;
}

fn calc_odds(throw_needed:i32, bonus: i32) -> i32 {
    let roll_needed = throw_needed - bonus;
    match roll_needed {
        2 => 100,
        3 => 97,
        4 => 92,
        5 => 83,
        6 => 72,
        7 => 58,
        8 => 41,
        9 => 28,
        10 => 17,
        11 => 8,
        12 => 3,
        _ => 0
    }
}

impl Career for UPP {
    fn rank_navy(&self) -> i32 {
        let mut rank = 0;
        let mut bonus = 0;
        // Enlistment
        if self.intelligence >= 8 {
            bonus += 1;
        }
        if self.education >= 9 {
            bonus += 2;
        }
        rank += calc_odds(8, bonus);
        // Survival
        if self.intelligence >= 7 {
            bonus = 2;
        }
        rank += calc_odds(5, bonus);
        return rank;
    }
    fn rank_marines(&self) -> i32 {
        let mut rank = 0;
        let mut bonus = 0;
        // Enlistment
        if self.intelligence >= 8 {
            bonus += 1;
        }
        if self.strength >= 8 {
            bonus += 2;
        }
        rank += calc_odds(9, bonus);
        // Survival
        if self.endurance >= 8 {
            bonus = 2;
        }
        rank += calc_odds(6, bonus);
        return rank;
    }
    fn rank_army(&self) -> i32 {
        let mut rank = 0;
        let mut bonus = 0;
        // Enlistment
        if self.dexterity >= 6 {
            bonus += 1;
        }
        if self.endurance >= 5 {
            bonus += 2;
        }
        rank += calc_odds(5, bonus);
        // Survival
        if self.education >= 6 {
            bonus = 2;
        }
        rank += calc_odds(5, bonus);
        return rank;
    }
    fn rank_scouts(&self) -> i32 {
        let mut rank = 0;
        let mut bonus = 0;
        // Enlistment
        if self.intelligence >= 6 {
            bonus += 1;
        }
        if self.strength >= 8 {
            bonus += 2;
        }
        rank += calc_odds(7, bonus);
        // Survival
        if self.endurance >= 9 {
            bonus = 2;
        }
        rank += calc_odds(7, bonus);
        return rank;
    }
    fn rank_merchant(&self) -> i32 {
        let mut rank = 0;
        let mut bonus = 0;
        // Enlistment
        if self.strength >= 7 {
            bonus += 1;
        }
        if self.intelligence >= 6 {
            bonus += 2;
        }
        rank += calc_odds(7, bonus);
        // Survival
        if self.intelligence >= 7 {
            bonus = 2;
        }
        rank += calc_odds(5, bonus);
        return rank;
    }
    fn rank_other(&self) -> i32 {
        let mut rank = 0;
        let mut bonus = 0;
        // Survival
        if self.intelligence >= 9 {
            bonus = 2;
        }
        rank += calc_odds(5, bonus);
        return rank;
    }
    fn enlist(&self, career: CareerType, rng: &mut ThreadRng) -> CareerType {
        match career {
            CareerType::Navy => {
                let mut bonus = 0;
                if self.intelligence >= 8 {
                    bonus += 1;
                }
                if self.education >= 9 {
                    bonus += 2;
                }
                let roll = throw_die(rng) + throw_die(rng) + bonus;
                if roll >= 8 {
                    println!("Enlisted in the Navy!");
                    return CareerType::Navy;
                }
                else {
                    println!("FAILED to enlist in the Navy...");
                    return Self::draft(rng);
                }
            }
            CareerType::Marines => {
                let mut bonus = 0;
                if self.intelligence >= 8 {
                    bonus += 1;
                }
                if self.strength >= 8 {
                    bonus += 2;
                }
                let roll = throw_die(rng) + throw_die(rng) + bonus;
                if roll >= 9 {
                    println!("Enlisted in the Marines!");
                    return CareerType::Marines;
                }
                else {
                    println!("FAILED to enlist in the Marines...");
                    return Self::draft(rng);
                }                
            }
            CareerType::Army => {
                let mut bonus = 0;
                if self.dexterity >= 6 {
                    bonus += 1;
                }
                if self.endurance >= 5 {
                    bonus += 2;
                }
                let roll = throw_die(rng) + throw_die(rng) + bonus;
                if roll >= 5 {
                    println!("Enlisted in the Army!");
                    return CareerType::Army;
                }
                else {
                    println!("FAILED to enlist in the Army...");
                    return Self::draft(rng);
                }                
            }
            CareerType::Scouts => {
                let mut bonus = 0;
                if self.intelligence >= 6 {
                    bonus += 1;
                }
                if self.strength >= 8 {
                    bonus += 2;
                }
                let roll = throw_die(rng) + throw_die(rng) + bonus;
                if roll >= 7 {
                    println!("Enlisted in the Scouts!");
                    return CareerType::Scouts;
                }
                else {
                    println!("FAILED to enlist in the Scouts...");
                    return Self::draft(rng);
                }                
            }
            CareerType::Merchants => {
                let mut bonus = 0;
                if self.strength >= 7 {
                    bonus += 1;
                }
                if self.intelligence >= 6 {
                    bonus += 2;
                }
                let roll = throw_die(rng) + throw_die(rng) + bonus;
                if roll >= 7 {
                    println!("Enlisted in the Merchants!");
                    return CareerType::Merchants;
                }
                else {
                    println!("FAILED to enlist in the Merchants...");
                    return Self::draft(rng);
                }
            }
            _ => {
                let roll = throw_die(rng) + throw_die(rng);
                if roll >= 3 {
                    println!("Enlisted in Other!");
                    return CareerType::Merchants;
                }
                else {
                    println!("FAILED to enlist in Other...");
                    return Self::draft(rng);
                }                
            }
        }
    }
    fn draft(rng: &mut ThreadRng) -> CareerType {
        match throw_die(rng) {
            1 => CareerType::Navy,
            2 => CareerType::Marines,
            3 => CareerType::Army,
            4 => CareerType::Scouts,
            5 => CareerType::Merchants,
            _ => CareerType::Other
        }
    }
}

#[derive(Display, Debug, EnumString, PartialEq, Eq, Hash)]
#[strum(serialize_all)]
enum CareerType {
    Navy,
    Marines,
    Army,
    Scouts,
    Merchants,
    Other
}

fn select_career(upp: &UPP) -> CareerType {
    let mut career = CareerType::Other;

    let mut options = HashMap::new();

    options.insert(CareerType::Navy, upp.rank_navy());
    options.insert(CareerType::Marines, upp.rank_marines());
    options.insert(CareerType::Army, upp.rank_army());
    options.insert(CareerType::Army, upp.rank_scouts());
    options.insert(CareerType::Merchants, upp.rank_merchant());
    options.insert(CareerType::Other, upp.rank_other());

    println!("    Options =>");
    for (name, value) in &options {
        println!("      {}: {}", name, value);
    }

    let mut max = i32::min_value();

    for (career_name, career_rank) in options {
        if max < career_rank {
            max = career_rank;
            career = career_name;
        }
    }

    return career;
}

fn main() {
    let mut rng = thread_rng();

    println!("Marc Miller's Classic Traveller");

    let upp = UPP {
        strength: throw_die(&mut rng) as i32 + throw_die(&mut rng) as i32,
        dexterity: throw_die(&mut rng) as i32 + throw_die(&mut rng) as i32,
        endurance: throw_die(&mut rng) as i32 + throw_die(&mut rng) as i32,
        intelligence: throw_die(&mut rng) as i32 + throw_die(&mut rng) as i32,
        education: throw_die(&mut rng) as i32 + throw_die(&mut rng) as i32,
        social: throw_die(&mut rng) as i32 + throw_die(&mut rng) as i32
    };

    println!("UPP: {}", upp);
    println!("\tStrength       : {}", upp.strength);
    println!("\tDexterity      : {}", upp.dexterity);
    println!("\tEndurance      : {}", upp.endurance);
    println!("\tIntelligence   : {}", upp.intelligence);
    println!("\tEducation      : {}", upp.education);
    match upp.social {
        11 => println!("\tSocial Standing: {}    Knight, Knightess, Dame",
            upp.social),
        12 => println!("\tSocial Standing: {}    Baron, Baronet, Baroness",
            upp.social),
        13 => println!("\tSocial Standing: {}    Marquis, Marquesa, Marchioness",
            upp.social),
        14 => println!("\tSocial Standing: {}    Count, Countess", upp.social),
        15 => println!("\tSocial Standing: {}    Duke, Duchess", upp.social),
        _ => println!("\tSocial Standing: {}", upp.social)
    }

    println!("    Hits: {}", upp.calc_hits());

    println!("Career Selection");
    let career_selected = select_career(&upp);
    println!("Selection = {}", career_selected.to_string());
    let career = upp.enlist(career_selected, &mut rng);
    println!("CAREER: {}", career);
}
